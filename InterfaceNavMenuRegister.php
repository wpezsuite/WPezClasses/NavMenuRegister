<?php

namespace WPezSuite\WPezClasses\NavMenuRegister;

interface InterfaceNavMenuRegister {

	public function registerNavMenu();

}