<?php

namespace WPezSuite\WPezClasses\NavMenuRegister;

// No WP? Die! Now!!
if (!defined('ABSPATH')) {
    header('HTTP/1.0 403 Forbidden');
    die();
}

class ClassNavMenuRegister implements InterfaceNavMenuRegister
{

    protected $_arr_menus;
    protected $_arr_menu_defaults;
    protected $_arr_ret;
    protected $_arr_ret_rnm;

    public function __construct()
    {

        $this->setPropertyDefaults();

    }

    protected function setPropertyDefaults()
    {

        $this->_arr_menus = [];
        $this->_arr_ret = [];
        $this->_arr_ret_rnm = [];

        $this->_arr_menu_defaults = [
            'active' => true,
            'loc' => false,
            'desc' => false
        ];

    }

    public function pushMenu($arr_menu = false)
    {
        if (!is_array($arr_menu)) {
            return false;
        }

        $arr_temp = array_merge($this->_arr_menu_defaults, $arr_menu);

        if (!is_string($arr_temp['loc']) || !is_string($arr_temp['desc'])) {
            return false;
        }

        $this->_arr_menus{$arr_temp['loc']} = $arr_temp;

        return true;
    }


    public function loadMenus($arr_menus = false)
    {

        if (!is_array($arr_menus)) {
            return false;
        }

        $this->_arr_ret = [];
        foreach ($arr_menus as $key => $arr_menu) {

            $this->_arr_ret[$key] = $this->pushMenu($arr_menu);
        }

        return $this->_arr_ret;

    }

    public function getMenus()
    {

        return $this->_arr_menus;
    }

    public function registerNavMenu()
    {
        foreach ( $this->_arr_menus as $arr_menu){

            if ( $arr_menu['active'] !== true ){
                $this->_arr_ret_rnm = [$arr_menu['loc']] = 'active = false';
                continue;
            }
            $this->_arr_ret_rnm[$arr_menu['loc']] = register_nav_menu($arr_menu['loc'], $arr_menu['desc'] );

        }
        return $this->_arr_ret_rnm;
    }


    public function getRegisterNavMenu(){

        return $this->_arr_ret_rnm;
    }
}
