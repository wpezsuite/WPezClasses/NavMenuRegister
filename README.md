## WPezClasses: Nav Menu Register

__WordPress register_nav_menu() done The ezWay.__
   
> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example

_Recommended: Use the WPezClasses autoloader (link below)._

```

use WPezSuite\WPezClasses\NavMenuRegister\ClassNavManuRegister as Register;
use WPezSuite\WPezClasses\NavMenuRegister\ClassHooks as Hooks;

$arr_menus = [
    'nav1' => [
        'active' => true,
        'loc' => 'header_main',
        'desc' => 'Header Main'
        ],
    'nav2' => [
        'active' => true,
        'loc' => 'footer_main',
        'desc' => 'Footer Main'
        ],
    ];
    
$arr_menu = [
        'active' => true,
        'loc' => 'header_mobile',
        'desc' => 'Header Mobile'
    ];
    
$new = new Register();
// load via array
$new->loadMenus($arr_menus);
// or push one at a time
$new->pushMenu($arr_menu);
    
$new_hooks = new Hooks($new);
$new_hooks->register();

```

### FAQ

__1) Why?__

Because it's wiser and ez'ier to configure an array than it is to hard-code functions. Arrays can be pulled from a library. They can be manipulated. They can be fed from a config file into some pre-tested boilerplate code. Etc.

__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 

 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit.

__4) But this is very similar to traditional WordPress.__ 

Similar yes. But keep in mind this brings nav menus inline with scripts, styles, images, etc. that already benefit from the magic of The ezWay. Menus is yet just another in your config / setup file. Less coding, more configuring.  


### Helpful Links

- https://developer.wordpress.org/reference/functions/register_nav_menu/

- https://developer.wordpress.org/reference/functions/wp_nav_menu/

- https://developer.wordpress.org/reference/functions/wp_get_nav_menu_items/

### TODO

n/a

### CHANGE LOG

- v0.0.2 - Thursday 12 September 2019
    - FIXED - Wrongly placed = (equals sign) in the method: registerNavMenu().

- v0.0.1 - Thursday 12 September 2019
    - Hey! Ho!! Let's go!!! Yet another "finally doing a proper repo of this tool." Please pardon the delay.